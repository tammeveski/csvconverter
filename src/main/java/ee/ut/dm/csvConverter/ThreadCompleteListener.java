package ee.ut.dm.csvConverter;

public interface ThreadCompleteListener {
	
	public void notifyComplete();

}
