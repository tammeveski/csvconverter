package ee.ut.dm.csvConverter;

import java.util.List;
import java.util.Map;

public class Parser implements Runnable {

	private String path = "";
	private List<String> featureList;
	private Map<String, Integer> featureSet;
	private ThreadCompleteListener threadCompleteListener;
	
	public Parser(List<String> featureList, Map<String, Integer> featureSet, String path, ThreadCompleteListener threadCompleteListener) {
		this.path = path;
		this.featureList = featureList;
		this.featureSet = featureSet;
		this.threadCompleteListener = threadCompleteListener;
	}
	
	public final void run() {
		try {
			CsvIO.convertCsv(path, featureList, featureSet);
		} finally {
			threadCompleteListener.notifyComplete();
		}
	}
}
