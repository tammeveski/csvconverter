package ee.ut.dm.csvConverter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvConverter implements ThreadCompleteListener {
	private final int MAX_THREADS = 4;
	private List<Thread> parserThreads = new ArrayList<Thread>();
	private int runningThreads = 0;

	public static void main(String[] args) {
		CsvConverter converter = new CsvConverter();
		boolean createHeader = true;
		boolean createIds = true;
		
		if(args.length < 2) {
			printUsage();
			System.exit(-1);
		}
		
		List<String> arguments = new ArrayList<String>(Arrays.asList(args));
		
		if(arguments.contains("-existingHeader")) {
			createHeader = false;
		}
		
		if(arguments.contains("-existingIds")) {
			createIds = false;
		}
				
		int numParts = 0;
		int minCount = 0;
		
		try {
			numParts = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("Unable to parse number of parts!");
			printUsage();
		}
		
		if(arguments.contains("-randomSample")) {
			CsvIO.randomSample(numParts, 100000);
			System.exit(0);
		}
		
		try {
			minCount = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.out.println("Unable to parse minimum number of occurences of a feature!");
			printUsage();
		}
		
		converter.convert(createHeader, createIds, numParts, minCount);
	}

	private static void printUsage() {
		System.out.println("Usage:");
		System.out.println("CsvConverter numParts minNumberOfOccurences [-existingHeader] [-existingIds] [-randomSample]");
		System.out.println("* numParts - number of parts that the csv is split into. Files must be named crm_anon.000 - crm_anon.nnn");
		System.out.println("    Both the parts and the original file crm_anon.csv must be located under the folder 'input'");
		System.out.println("* minNumberOfOccurences - number of times that a feature must appear for it to be included in the output");
		System.out.println("* -existingHeader - include this flag to skip feature identification and use an existing header file from output folder");
		System.out.println("* -existingIds - include this flag to skip generation of id-s for input data");
		System.out.println("Output will be stored under the folder 'output'");
	}

	private void convert(boolean createHeader, boolean createIds, int numParts, int minCount) {
		if(createHeader) {
			List<String> featureList = CsvIO.findAllFeatures("input/crm_anon.csv", minCount);
			CsvIO.writeFeatureHeader(featureList);
		}
		if(createIds) {
			CsvIO.createIds("input/crm_anon.csv");
		}
				
		List<String> featureList = CsvIO.importFeatureHeader();
		System.out.println("num features: " + featureList.size());
		Map<String, Integer> featureSet = new HashMap<String, Integer>();
		
		for(int i = 0; i < featureList.size(); i++) {
			featureSet.put(featureList.get(i), i);
		}
		
		for(int i = 0; i < numParts; i++) {
			String fileName = "crm_anon." + String.format("%03d", i);
			
			Parser parser = new Parser(featureList, featureSet, fileName, this);
			Thread parserThread = new Thread(parser);
			parserThreads.add(parserThread);
		}		

		for(int i = 0; i < MAX_THREADS; i++) {
			parserThreads.get(i).start();
			runningThreads++;
		}
		
		for(int i = 0; i < MAX_THREADS; i++) {
			try {
				parserThreads.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void notifyComplete() {
		runningThreads--;
		for(int i = 0; i < parserThreads.size(); i++) {
			if(parserThreads.get(i).getState() == Thread.State.NEW && runningThreads < MAX_THREADS) {
				parserThreads.get(i).start();
				runningThreads++;
			}
		}
	}
}
